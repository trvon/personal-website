---
layout: post
title:  "Survival Guide for International Instructors"
date:   2021-09-2 10:00:00 +0000
categories: workshop
---
This workshop was given during a teaching seminar by [Dr. Charles Hutchison](https://webpages.charlotte.edu/~chutchis/). The presentation is based on his book *"Experiences of Immigrant Professors"* and focuses on his research and personal experience of teaching in the American classroom.
