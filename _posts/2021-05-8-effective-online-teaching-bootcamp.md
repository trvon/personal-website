---
layout: post
title:  "Effective Online Teaching Bootcamp"
date:   2021-05-8 10:00:00 +0000
categories: workshop
---
*Effective Online Teaching Bootcamp* was an informative introduction to translating course goals into a structured and engaging canvas page. Many of the resources provided helped think about structuring the course from an online prospective relating to engaging students effectively. This included resources for active engagement resources like poll-everywhere, zoom and Miro and how you might structure that into your class format as well as resources for teaching online via online conferencing tools like zoom. The teaching session also provided a [checklist](https://teaching.uncc.edu/teaching-guides/checklist-online-courses) for online courses to make sure that the canvas page is engaging and easy to navigate.

## Resources
- [CTL YouTube Page](https://www.youtube.com/channel/UCyGdAZFMerajWb4ZBzvDEWA) 
- [UNC Charlotte FAQs (AKA Spaces)](https://spaces.uncc.edu/)
- [Canvas 24/7 Live Support](https://canvas.uncc.edu/canvas-support) 
