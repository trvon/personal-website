---
layout: page
title: Home
permalink: /
---

![Headshot](/assets/ava.jpg){: height="auto" width="150px" margin="auto" text-align="center"}

## About Me

My name is Trevon, and I am a PhD student at UNC Charlotte where I am conducting research and receiving teaching mentorship from Dr. Jinpeng Wei. I am researching applications of in-network solutions related to packet filtering and telemetry gathering to improve the performance and efficiency of security solutions.

### Research Topics

- In-Network/Programmable Security
- Packet Filters
- Distributed Systems

#### Additional Interesting

- Mobile Malware
- Machine Learning

## Find me on the Internet

- [Github](https://github.com/trvon)
- [Security Research Site](https://manta.black)
- Email me[@]trevon.dev
