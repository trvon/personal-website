---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title: Blog & Notes
permalink: /blog/
---
[RSS Feed](https://trevon.dev/rss.xml)

# Welcome
> I am glad that you have taken the time to visit my website. Here lies some of my research findings and thoughts that I felt comfortable sharing. I hope that you enjoy reading my posts and I look forward to any feedback.
