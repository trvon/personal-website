---
layout: post
title: Teaching
permalink: /milestones/
toc: true
---

## Spring 2025

** Goals in Progress **

## Fall 2024

**Goals**
* [ ] Submit a paper to [USENIX NSDI'25](https://www.usenix.org/conference/nsdi25/call-for-papers) or similar academic conference
* [ ] Progress Disseration Work
* [ ] Instructional Assistant (IA) for ITIS 6167 : Network Security

## Spring 2024

**Goals**
* [ ] Submit a paper to [USENIX NSDI'25](https://www.usenix.org/conference/nsdi25/call-for-papers) and [ACSAC 2024](https://www.acsac.org/)
* [x] Defend Proposal
* [x] Teach ITIS 3246

## Fall 2023

**Goals**
* [ ] Submit a paper to USENIX Security'24 or similar conference
* [x] Formalize training for [cyber security competitions](https://github.com/49thSecurityDivision/Competitions)
* [x] Revise ITIS 3246
