---
layout: page
title: Service 
permalink: /service/
---
## Volunteering

> [CarolinaCon Online](https://carolinacon.org/)
- Co-Organizer
- [CTF CC Online](https://github.com/49thSecurityDivision/CC-CTF-Online) Infrastructure Architect and Challenge Contributor
- [CTF CC Online 2](https://github.com/49thSecurityDivision/CC-CTF-Online-2) Infrastructure Architect and Challenge Contributor
- [CTF CC Online 3](https://github.com/49thSecurityDivision/CC-CTF-Online-3) Infrastructure Architect and Challenge Contributor

> [BIC Secret Con](https://www.blacksincyberconf.com/events-1) 2021 -- CTF Challenge Contributor

## Leadership Experience

### 2022 - 2023 (School Semester)

> [49th Security Division](https://49sd.com) -- Ethical Hacking Club
- Officer

### 2021 - 2022 (School Semester)

> [49th Security Division](https://49sd.com) -- Ethical Hacking Club
- Officer

### 2020 - 2021 (School Semester)

> [49th Security Division](https://49sd.com) -- Ethical Hacking Club
- Vice President
- Lab Manager
