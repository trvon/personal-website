---
layout: post
title: Teaching
permalink: /teaching/
toc: true
---
> *As a GAANN fellow at UNC Charlotte from 2019-2023, I was tasked with tracking my experience in academic course planning, mentorship and teaching experience. On this web page you will find an archive and synopsis for each attended workshop, teaching experience milestones, professional development, my teaching experiment plan and my teaching philosophy.*

# Teaching Philosophy
Teachers have facilitated learning for me in a plethora of ways. Some guided me to where I did not know I could go, others gave me space to realize the importance of the topic, and still others just gave me the cold hard facts. This experience has led me to believe two perceptions about the world of learning. Everyone can learn regardless of their perceived desire and teaching is just as much of a learning journey as the act of leading pupils to the fountain of knowledge.

Why do I believe that everyone can learn? From a psychological perspective, everyone’s brain has plasticity as a quality. Meaning, with enough practice and time, anyone can teach themselves to think in a specific way. I approach the classroom from this foundational perspective. Each student's performance reflects the quality of time they can spend on a subject. This can highlight either the lack of time management or the external circumstances that take priority. This complexity requires teachers to juggle students' openness with grace to identify how to better serve students who may not know how they need help.

As a teacher, there are limited opportunities to serve students. Acknowledging their ability to grow and directing them to opportunities can have lifelong positive effects. This point of intersection between the student and teacher responsibilities, facilitates a lifelong process of learning amongst them both.

Learning to teach effectively is not a natural skill, it must be cultivated. This process of learning requires intentional reflection on feedback and the ability to improve the quality of this gathered feedback. Robert Heilein famously said “‘When one teaches, two learn.”. As a teacher, I acknowledge that no two students will ever be the same. To me, this means that I will have to continuously evolve my own understanding of the art of teaching to better serve my students.

# Milestones

> Future milestones moved to [link](https://trevon.dev/milestones)


## Fall 2024

**Goals**
* [ ] Submit a paper to [USENIX NSDI'25](https://www.usenix.org/conference/nsdi25/call-for-papers) and another academic conference
* [ ] Progress Disseration Work
* [ ] Instructional Assistant (IA) for ITIS 6167 : Network Security

## Spring 2024

**Goals**
* [ ] Submit a paper to [USENIX NSDI'25](https://www.usenix.org/conference/nsdi25/call-for-papers) and [ACSAC 2024](https://www.acsac.org/)
* [x] Defend Proposal
* [x] Teach ITIS 3246

## Fall 2023

**Goals**
* [ ] Submit a paper to USENIX Security'24 or similar conference
* [x] Formalize training for [cyber security competitions](https://github.com/49thSecurityDivision/Competitions)
* [x] Revise ITIS 3246

### Teach Experiment Report and Beyond

After spring 2023, I am not formally required to provide a teaching experiment report but tracking my goals is a good practice I plan to continue. I will continue to keep the semester format and denote successfully met goals with [x]. To reflect on my year as a whole. It was a grind, but I am still kicking.

## Spring 2023

**Goals**
* Revise and re-submit research paper to NSDI'24
* Teach ITIS 3246 (3x)
* Work on dissertation proposal draft

### Teaching Experiment Report

This semester, I intended to make some major headway on my research and worked as the Professor of record for IT IS 3246, IT Infrastructure and Security. In relation to the goals, I had for the semester, I would say that I made some major headway on my research direction and attempted to improve the course material for IT IS 3246 to use docker instead of podman in hopes of reducing the number of issues students ran into. 

My research progress this semester was significant this semester. I made major updates to my network security control and evaluation environment. I moved from using [FOP4]( https://github.com/ANTLab-polimi/FOP4) to [P4APP]( https://github.com/p4lang/p4app) which enabled me to automate evaluation. I also improved the codebase for our p4 switch filter and our network controller to leverage libraries for testing and communication. This summer I plan to read the updated language specification [p4lang 16 1.2.4](https://p4.org/p4-spec/docs/P4-16-v1.2.4.pdf) to take advantage of any language improvements.

With regards to teaching, the work load of modify the course material was significantly reduced due the work I did last semester and from the great help I received from current and past TA’s.

## Fall 2022

**Goals**
* Preform a proposal defense
* Re-submit past conference paper
* Submit a new research paper
* Attend Tapia as a presenter

### Teaching Experiment Report

This semester was field with some detours, but I made some headway working on my proposal and setup frameworks for structuring adding future papers into the proposal. My initial goals for this semester were to do my proposal defense and to focus on research but that ended up getting postponed for an academic year.

An unexpected development this semester led me to make some major modifications to the course material as we transitioned the course from using LXD to podman. These modifications were both a good learning experience and tedious task. It also taught me allot about how to think about course materials and evaluations from a learning experience. It also showed me that regardless of how much you might prepare, you will always run into problems that you did not initially expect, and it is better to use stable software than a new solution. 

In relation to my research, I spent most of my time reading research papers and revising the how I described the systems that I am working on. I was not able to be as productive as I wanted this semester but was able to get reorganized and refocused.

## Spring 2022

**Goals**
* Complete paper of on-going work
* Begin working on proposal
* Teach ITIS 3245 (2x)

### Teaching Experiment Report

The spring 2022 semester was highlighted by first and new experiences. This semester, I taught my first in-person flipped class and submitted my first conference paper. In this small excerpt, I hope to highlight my experience this semester and how I met the goals that I set for this semester.

My first goal for this semester was to submit a paper on my ongoing work. My research focuses on leveraging programmable network security to improve the agility of computer networks and security policy. To accomplish this, we use the p4 language to create a stateful firewall capable of updating policy from a network application processing snort alerts. This allows the network architect to implement a control loop capable of handling the filtering of low-level network anomalies and detections given there is a source generating alerts for detections. I created a poster and presentation about this research for the University graduate symposium and submitted a paper in earlier April 2022.

After completing reaching the milestone of documenting this research, I started working on a proposal to outline the path I will attempt to take for my dissertation work. At a high-level, network automations for security solutions provide great benefits as well as introduce concerns. My work will attempt to show the benefits and propose a solution for dealing with security concerns that relate to the potential effects of a comprised network controller and the defenses present in network controllers. We hope this solution reduces the concerns of deploying automation programmability in the network environment. I hope this summer, that I can polish this ongoing research to deliver a proposal defense in the Fall.


## Fall 2021

**Goals**
* Teach ITIS 3246
* Complete Teaching Seminar course x 4 (ITCS 8665)
* Submit at least one paper to a conference
* Begin work on proposal and outline second paper for submission
* Attend at least one Learning Center Seminar/Workshop

**Workshops**
- Survival Guide for International Instructors (9/2/2021)

### Teaching Experiment Report

Fall 2021 was a semester filled with as much challenge as it was filled with promise. This semester, as I transferred back to working on campus and focused on completing my course work, teaching ITIS 3246, and working on a conference paper. In this write up, I reflect on what I have learned, what I accomplished and the goals I have left to accomplish.

This semester was the first semester that I took on the full teaching responsibilities for ITIS 3246, IT Infrastructure and Security. Teaching this course required the most of my time and taught me important lessons about time management and time allocation to students. I conducted this class fully remote, and I used Campuswire, email and zoom to communicate with students and to conduct office hours and active-learning sessions. I also helped with shaping course material, student grading, and managing TA responsibilities.

I began work on a paper titled “An Approach Towards an SDN Security Framework” that leverages the P4 programming language to distributed security utilities into hardware switches. I made great progress on this paper but had to revise and focus the product of the research to a smaller contribution than initially planned. Going forward, I plan to continue working on this paper, with the hope of finish at the beginning of the semester given that I have completed all my core courses.


## Spring 2021

**Goals**
* Complete Teaching Seminar course x 3 (ITCS 8665)
* Instructor of Record of IT Infrastructure, ITIS 3246
* Attend at least one Learning Center Seminar/Workshop
* Submit at least one paper to a conference
* Complete Qualifier Exam

### Teaching Experiment Report

During the Spring 2021 semester, I focused on preparing to solo teach IT IS 3246, completing my qualifier exam, writing a paper, and contributing in my spare time to events in my specialty.  In this section, I will provide a summary of each milestone met and some of the shortcomings that I faced.

I assisted ITIS 3246 as an assistant to the professor while fulfilling my TA responsibilities. This included interfacing and helping other TA’s, contributing to course content improvements and hosting a class. I also plan to attend a learning workshop May 11, 2021, and I will link my notes [here](https://trevon.dev/workshop/2021/05/08/effective-online-teaching-bootcamp.html).

Regarding my qualifier exam, I successfully completed the requirements April 8, 2021. My qualifier presentation was titled, “A Review of Network Advancements for Future Adaptive Systems” and my literature review can be found [here](https://trevon.dev/papers/lit_review.pdf). After completing my qualifier, I have begun a draft for a future series of conference papers that I hope to have submitted by January 2022. 

Finally, I made meaningful contributions to helping organize and host both the CarolinaCon conference and CTF. This both allowed me to contribute my time to the information security community and provided me with hands-on experience as I helped build and maintain an online [capture the flag](https://ctfd.io/whats-a-ctf/) (CTF) competition that spanned the weekend of April 25, 2021. 

This semester was filled with many challenges and successes and I hope that was communicated succinctly here.

## Fall 2020

 **Goals**
* Complete Teaching Seminar course x 2 (ITCS 8665)
* Support teaching mentor in IT Infrastructure (3246 Course) [Support includes guest lecturing and preparing course material]
* Attend at least one Learning Center Seminar/Workshop
* Complete Qualifier Exam

**Workshops**
- Online Teaching Foundations (11/10/2020)

### Teaching Experiment Report

Fall 2020, I focused on finishing my literature review in hope to complete my QE. Unfortunately, I did not achieve this goal. Alternatively, I made more meaningful connections in my literature review and I hope to complete my qualifier during the Spring 2021 semester. Amongst my other documented goals, I was able to attend an online learning center workshop online, “Online Teaching Foundations”. I also learned many hard lessons surrounding availability and interaction management as I supported Dr. Moyer in IT Infrastructure. In this specific section of my teaching experiment report, I take the time to focus specifically on my lessons learned from the successful completion delay of this semester's goals.

The Online Teaching Foundations workshop was incredibly helpful and provided insight into course design, focused on online delivery. Notable topics that expanded my understanding of course design include backward design, utilizing different methods of assessments, and course pacing. The workshop provided many resources that I am working to store in a safe place for later references.

Being a TA for IT Infrastructure, this semester was more tedious and stressful than in semesters prior. Lessons I hard to learn the hard way included, not having email or discord installed on my phone and setting hours aside that I could take time away from helping students. Many students required one-on-one help in office hours and others found it more convenient to email or instant message me asking for specific help on assignments. Responding became drawing as communications came in at all hours of the day and night.

I believe that this semester helped me to grow to appreciate a health work-life balance and well-structured course material to add in helping students find answers outside of going to TA or instructors for immediate help. More than anything, I plan on taking the soft skills and wisdom learned from this semester into the next semesters of my studies.

## Spring 2020

 **Goals**
* Complete Teaching Seminar course x 1 (ITCS 8665)
* Gain more experience preparing class materials
* Support teaching mentor in IT Infrastructure (3246 Course)
* Attend at least one Learning Center Seminar/Workshop

**Workshops**
- Achieving the Essentials for Assessing Student Learning (4/8/2020)
- Mapping Student Workload for Online Activities and Assessments (4/9/2020)

### Teaching Experiment Report

During my spring 2020 semester, I achieved all of my documented goals. These goals include: *Completing the teaching seminar course, preparing class materials, supporting my teaching mentor and attending at least one learning center workshop*. The purpose of these goals where to ensure that I balanced my research efforts with complementary teaching experience. In this report, I summarize and highlight my takeaways from this semester.

The week of April 8th, I attended two teaching workshops that were focused on online delivery. The first workshop, "*Achieving the Essentials for Assessing Student Learning*", focused on student assessment adapting course assessment when transitioning from the traditional to online format. This workshop presented a methodology to help adapt classes based on the general course goals. The instructor provided three questions for adapting courses to better help students demonstrate their learning:

* What are you learning outcomes?
* What goals have and have not been assessed?
* Which goals are the most essential?
These questions are essential in helping to navigate unforeseeable and unmanageable change like the Covid-19 pandemic.

The second workshop focused on mapping student workloads in online activities. This workshop broke down course elements to help attendees understand how to structure and outline a course workload around a tool. In the demonstration of the tool, we can see how a Professor can better map course elements to a sample daily work map that groups activities into time blocks and helps guide the format of learning objectives and assessment.

The Teaching Seminar Course helped me gain experience preparing class materials as well as provided useful strategies to approaching a classroom environment. The Teaching Seminar also complemented my experience supporting my teaching mentor, Professor Moyer, for his IT Infrastructure course. To adequately support my teaching mentor as a TA; I held office hours once a week, graded assignments, and helped students during the lecture time.

This semester provided me with teaching and mentoring experience in and out of the classroom. I learned first-hand that teaching doesn't stop at the classroom and truly extends throughout the week and outside business hours. For example, when I was up late working on assignments, I seldom found myself getting messaged by students on discord asking for assistance. From this experience, I learned that it is helpful to set a work timetable for myself that guides when I do and do not make time to help students.

## Fall 2019

**Goals**
* Complete personal website and teaching portfolio
* Finish Master's thesis
* Attend at least one Learning Center Seminar/Workshop

**Workshops**
- Poll Everywhere Teaching Seminar (11/13/2019)

### Teaching Experiment Report

Fall 2019 was the first semester that I began as a PhD candidate and GAANN fellow at UNC Charlotte. For this semester, I generated three goals: to create my personal website and teaching portfolio, to finish my master’s thesis, and to attend at least one learning center seminar. These goals were created to help me establish a good foundation to build upon in future academic semesters.

The first and easiest to complete milestone was my website. I entered the program already having a website and figured that this would be the best place to start. By using Jekyll, I updated my website to include a home page, teaching portfolio page, and public release page
This structure can be seen in my current site.

The next task goal that I met was attending the Poll Everywhere teaching seminar. The poll everywhere seminar that I attended providing some guidance for how to utilize poll everywhere, an online quiz platform to aid active learning. In the seminar we learned about strategies to create and use poll’s for multiple classes, how to embed them into a diverse offering of presentation formats, and how to retrieve and export poll results into canvas.

The last goal that I had to meet, was met December 6. This milestone was the most tedious and the most strenuous to complete. I submitted my master’s thesis titled, “An Approach Towards an Autonomic Computing Prototype Reference Architecture”. In the paper, I outline the prototype that I seek to create in my future work and the design strategies that I will focus on. 

Each of these goals are integral for complete my PhD. This fall 2019 semester, I learned the importance of a PhD and how their work intersects with the world in ITCS 8110. The poll everywhere seminar equipped me with the tools I need to lead a classroom. The website is a landing page for my research, portfolio and research interests. Each of these goals represent components I strive to develop as I develop my teaching philosophy and resume in the coming semester.
